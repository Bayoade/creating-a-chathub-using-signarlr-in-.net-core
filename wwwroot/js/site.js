﻿const connection = new signalR.HubConnectionBuilder()
    .withUrl("/chathub")
    .build();

var handleClickEvent = function (event) {
    const user = document.getElementById("userInput").value;
    const message = document.getElementById("messageInput").value;

    connection.invoke("SendMessage", user, message).catch(err => {
        console.error(err.toString());
    });
    event.preventDefault();

};

//receive message
connection.on("ReceiveMessage", (user, message) => {
    const list = document.createElement("li");
    list.textContent = user + " : " + message;
    document.getElementById("messagesList").appendChild(list);


});

//send message
document.getElementById("sendButton").addEventListener("click", handleClickEvent);

connection.start().catch(err => {
    console.error(err.toString());
});
